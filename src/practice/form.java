package practice;

import javax.swing.*;
import java.io.FileNotFoundException;
import java.util.Arrays;
import java.awt.event.*;

public class form extends JFrame {
    private JPanel panel1;


    private JButton Output;
    private JButton ImputAnalyzed;
    private JButton ImputSemantic;
    private JTextArea SemanticTextArea;
    private JTextArea BlackSemanticTextArea;
    private JTextArea OutputTextArea;
    private JTextArea AnalyzedTextArea;
    private JPanel MainPanel;


    public form() {
        setContentPane(panel1);
        setTitle("выход");
       setSize(600,500);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        ImputAnalyzed.addActionListener(new InputAnalyzed());
        Output.addActionListener(new Output());
        ImputSemantic.addActionListener(new InputSemantic());

      AnalyzedTextArea.setLineWrap(true);
      AnalyzedTextArea.setWrapStyleWord(true);
      OutputTextArea.setLineWrap(true);
      OutputTextArea.setWrapStyleWord(true);
      SemanticTextArea.setLineWrap(true);
        SemanticTextArea.setWrapStyleWord(true);
        BlackSemanticTextArea.setLineWrap(true);
                BlackSemanticTextArea.setWrapStyleWord(true);
    }

    private void createUIComponents() {
        panel1 = new JPanel();
        Output = new JButton();
        ImputAnalyzed = new JButton();
        ImputSemantic = new JButton();
    }

    class Output implements ActionListener {
        String[] VoidArray = {""};
        public void actionPerformed(ActionEvent e) {
            String message ="";
            main.OutputArray = comparison.words(main.SemanticArray, main.AnalyzedArray);
            if (Arrays.equals(main.OutputArray, VoidArray)) {
                message +="количество совпадений: "+ "0"+"\n";
            }
            else {
                message +="количество совпадений: "+ main.OutputArray.length+"\n";
            }
            message =message + Arrays.toString(main.OutputArray);
            OutputTextArea.setText(null);
            OutputTextArea.append(message);
                message ="количество слов: " + comparison.BlackSemantic(main.OutputArray).length+"\n";
                message +=Arrays.toString(comparison.BlackSemantic(main.OutputArray));
            BlackSemanticTextArea.setText(null);
            BlackSemanticTextArea.append(message);
        }

    }

    class InputAnalyzed implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            JFileChooser fileopen = new JFileChooser();
            int ret = fileopen.showDialog(null, "Открыть файл");
            if (ret == JFileChooser.APPROVE_OPTION) {
                main.AnalyzedFile = fileopen.getSelectedFile();
                try {
                    main.AnalyzedArray = words.NewWord(FileRead.read(main.AnalyzedFile));
                } catch (FileNotFoundException ex) {
                    ex.printStackTrace();
                }
                AnalyzedTextArea.setText(null);
                AnalyzedTextArea.append(Arrays.toString(main.AnalyzedArray));

        }
    }

                }
                class InputSemantic implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            JFileChooser fileopen = new JFileChooser();
            int ret = fileopen.showDialog(null, "Открыть файл");
            if (ret == JFileChooser.APPROVE_OPTION) {

                main.SemanticFile = fileopen.getSelectedFile();
                try {
                    main.SemanticArray = words.NewWord(FileRead.read(main.SemanticFile));
                } catch (FileNotFoundException ex) {
                    ex.printStackTrace();
                }

                    SemanticTextArea.setText(null);
                    SemanticTextArea.append(Arrays.toString(main.SemanticArray));
            }

        }
    }
}
